import { Component, OnInit } from '@angular/core';
import { Bolo } from '../bolo';
import { GetbolosService } from '../getbolos.service';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-paginapedido',
  templateUrl: './paginapedido.page.html',
  styleUrls: ['./paginapedido.page.scss'],
})
export class PaginapedidoPage implements OnInit {

  bolo: Bolo = {};
  id: string;

  constructor(private getBolosService: GetbolosService, private activatedRote: ActivatedRoute, private navController: NavController) {
    this.activatedRote.params.subscribe(params => {
      this.id = params['id'];
      this.getBolosService.getBoloById(this.id).subscribe((bolo) => {
        this.bolo = bolo;
      })
    });
  }

  ngOnInit() {
    
  }

  addCarrinho() {
    this.getBolosService.addCarrinho(this.bolo).then(() => {
      this.navController.pop();
    })
  }
}
