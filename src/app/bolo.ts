export interface Bolo {
    id?: string;
    nome?: string;
    descricao?: string;
    img?: string;
    preco?: number;
}
